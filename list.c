/*
 * This file is part of dscomp-list.
 * 
 * Copyright 2014 Karl Lindén <lilrc@users.sourceforge.net>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

/* perror() */
#include <stdio.h>

/* free(), malloc() */
#include <stdlib.h>

#include "list.h"

static list_node_t *
list_node_new ( const int key, const int value )
{
	list_node_t * const node = malloc(sizeof(list_node_t));
	if ( node == NULL ) {
		perror("malloc");
		return NULL;
	}

	node->key = key;
	node->value = value;
	node->next = NULL;

	return node;
}

static void __attribute__((nonnull))
list_node_destroy ( list_node_t * const node )
{
	free(node);
	return;
}

static void
list_node_destroy_all ( list_node_t * node )
{
	while ( node != NULL ) {
		list_node_t * const next = node->next;
		list_node_destroy(node);
		node = next;
	}
	return;
}

#if RECYCLE
static void __attribute__((nonnull))
list_recycle_all ( list_t * const list )
{
	list_node_t * const first = list->first;
	list_node_t * const last = list->last;
	last->next = list->recycled;
	list->recycled = first;
	return;
}
#endif /* RECYCLE */

list_t *
list_new ( void )
{
	list_t * const list = malloc(sizeof(list_t));
	if ( list == NULL ) {
		perror("malloc");
		return NULL;
	}

	list->first = NULL;

#if RECYCLE
	list->last = NULL;
	list->recycled = NULL;
#endif /* RECYCLE */

	return list;
}

void __attribute__((nonnull))
list_destroy ( list_t * const list )
{
	list_node_destroy_all(list->first);

#if RECYCLE
	list_node_destroy_all(list->recycled);
#endif /* RECYCLE */

	free(list);
	return;
}

list_error_t __attribute__((nonnull))
list_insert ( list_t * const list, const int key, const int value )
{
	if ( list->first == NULL ) {
		list->first = list_node_new(key, value);
		if ( list->first == NULL ) {
			return LIST_NOMEM;
		}
#if RECYCLE
		list->last = list->first;
#endif /* RECYCLE */
		return LIST_OK;
	} else if ( list->first->key > key ) {
		list_node_t * const node = list_node_new(key, value);
		if ( node == NULL ) {
			return LIST_NOMEM;
		}

		node->next = list->first;
		list->first = node;
		return LIST_OK;
	} else {
		list_node_t * prev = list->first;
		while ( prev->next != NULL && prev->next->key < key ) {
			prev = prev->next;
		}

		if ( key == prev->key ) {
			return LIST_EXISTS;
		}

		list_node_t * const next = prev->next;
		if ( next != NULL ) {
			if ( key == next->key ) {
				return LIST_EXISTS;
			} else {
				list_node_t * const node = list_node_new(key, value);
				if ( node == NULL ) {
					return LIST_NOMEM;
				}
				node->next = next;
				prev->next = node;
				return LIST_OK;
			}
		} else /* if ( next == NULL ) */ {
			list_node_t * const node = list_node_new(key, value);
			if ( node == NULL ) {
				return LIST_NOMEM;
			}
			prev->next = node;
#if RECYCLE
			list->last = node;
#endif /* RECYCLE */
			return LIST_OK;
		}
	}
}

int __attribute__((nonnull))
list_search ( const list_t * const list,
              const int key,
              int * const valuep )
{
	const list_node_t * node = list->first;
	while ( node != NULL ) {
		if ( key > node->key ) {
			node = node->next;
		} else if ( key == node->key ) {
			*valuep = node->value;
			return 0;
		} else /* if ( key < node->key ) */ {
			return 1;
		}
	}
	return 1;
}

void __attribute__((nonnull))
list_clear ( list_t * const list )
{
#if RECYCLE
	list_recycle_all(list);
#else /* !RECYCLE */
	list_node_destroy_all(list->first);
#endif /* !RECYCLE */

	list->first = NULL;
	return;
}

int __attribute__((nonnull))
list_check ( const list_t * const list )
{
	const list_node_t * prev = list->first;
	const list_node_t * node = prev->next;
	while ( node != NULL ) {
		if ( prev->key >= node->key ) {
			return 1;
		}
		prev = node;
		node = node->next;
	}
	return 0;
}
