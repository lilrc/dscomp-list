#  
#  This file is part of dscomp-list.
#  
#  Copyright (C) 2014 Karl Lindén <lilrc@users.sourceforge.net>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#                                               -*- Autoconf -*-
# Process this file with autoconf to produce a configure script.

AC_PREREQ([2.69])
AC_INIT(
	[dscomp-list],
	[1.0],
	[https://bitbucket.org/lilrc/dscomp-list/issues],
	[dscomp-list],
	[https://bitbucket.org/lilrc/dscomp-list])
AC_CONFIG_SRCDIR([module.c])
AC_CONFIG_HEADERS([config.h])
AC_CONFIG_MACRO_DIR([m4])

# The modules are shared libraries so building static libraries makes no
# sense.
LT_INIT([disable-static])

AM_INIT_AUTOMAKE

# Checks for programs.
AC_PROG_CC
AC_PROG_CC_C99
AS_VAR_IF(
	[ac_cv_prog_cc_c99],
	[no],
	[AC_MSG_ERROR([A C99 compatible C compiler is necessary])])

AM_PROG_CC_C_O

# Checks for header files.

# Checks for typedefs, structures, and compiler characteristics.

# Checks for library functions.
AC_FUNC_MALLOC

AC_ARG_ENABLE(
	[werror],
	[AS_HELP_STRING(
		[--enable-werror],
		[Pass -Werror when appropriate (default is no)])],
	[enable_werror=$enableval],
	[enable_werror=no]
)
AM_CONDITIONAL([ENABLE_WERROR], [test x$enable_werror = xyes])

DSCOMP_INIT

AC_CONFIG_FILES([Makefile])
AC_OUTPUT
