/*
 * This file is part of dscomp-list.
 * 
 * Copyright 2014 Karl Lindén <lilrc@users.sourceforge.net>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

#ifndef LIST_H
# define LIST_H

/* Define to recycle destroyed nodes to avoid unnecessary calls to
 * malloc(). */
# if !defined(RECYCLE)
#  define RECYCLE 0
# endif /* !defined(RECYCLE) */

enum list_error_e {
	LIST_OK,
	LIST_EXISTS,
	LIST_NOMEM
};
typedef enum list_error_e list_error_t;

struct list_node_s {
	int key;
	int value;
	struct list_node_s * next;
};
typedef struct list_node_s list_node_t;

struct list_s {
	list_node_t * first;
# if RECYCLE
	list_node_t * last;
	list_node_t * recycled;
# endif /* RECYCLE */
};
typedef struct list_s list_t;

list_t * list_new ( void );
void list_destroy ( list_t * const list ) __attribute__((nonnull));

list_error_t list_insert ( list_t * const list,
                           const int key,
                           const int value )
	__attribute__((nonnull));

int list_search ( const list_t * const list,
                  const int key,
                  int * const valuep )
	__attribute__((nonnull));

void list_clear ( list_t * const list ) __attribute__((nonnull));

int list_check ( const list_t * const list )
	__attribute__((nonnull));

#endif /* !LIST_H */
